# Project for Web Information Systems

## Introduction

This project aims to collect data from Wikipedia pages about animals. This a **crawler** with a client and a server. The client proposes a design interface in order to recolt input from the user and the server is used to make the crawler run. 

The crawler has been made from scratch and some latency may be observed but any observations to make it cleaner and faster are welcome.

## Setup

This project has been made whitin the **node** environment. In order to be able to run the project, first install all the dependencies **node** and its package manager, **npm**, may have. For further instructions about how to install it refer to the following link : <https://github.com/creationix/nvm>.

The client is made with React.js and the server with Express.js but these are details. If you want to run the project, first go directly within the project folder and run the commands below :

```
cd /client
npm install
cd ../server
npm install
```

These commands aim to install all the dependencies of the project. In order to run the application, we will need to firstly launch a proxy since the project is only available in developpment mode. In order to run this proxy, we will use `pm2`. After launching the proxy we can run the server and the client !

```
sudo npm i -g pm2
pm2 start proxy.js
npm start
cd ../client
npm start
```

Go to your <localhost:8000> to see the app
