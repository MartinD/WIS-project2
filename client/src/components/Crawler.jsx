import * as React from "react";
import { connect } from "react-redux";
import {Status} from "../actions/Status" 
import {
    Form,
    Input,
    Tooltip,
    Icon,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    AutoComplete,
    InputNumber,
    Card,
    Spin
} from "antd";
import { fetchAnimals } from "../actions/Animals";
import openSocket from 'socket.io-client';
//import {Crawler} from "../tools/crawler"

// const socket = openSocket('http://localhost:8081');

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;

export const CrawlerFormPage = connect(
    state => {
        return {
            animals: state.animals
        };
    },
    { fetchAnimals }
)(
    Form.create()(
        class CrawlerForm extends React.Component {
            constructor() {
                super();
                this.state = {
                    confirmDirty: false,
                    autoCompleteResult: [],
                    crawled: false,
                    animals: []
                };
            }

            // generateTree(data, cb){
            //     socket.on("animals", animals => cb(null, animals));
            //     socket.emit("generateTree", data)
            // }

            handleSubmit = e => {
                e.preventDefault();
                this.props.form.validateFieldsAndScroll((err, values) => {
                    if (!err) {
                        console.log("Received values of form: ", values);
                        // this.generateTree(values, (err, animals) => {
                        //     console.log(animals)
                        // })
                        //var crawler = new Crawler(values.url, values.depth, values.precision);
                        this.props.fetchAnimals(values)
                    }
                });
            };

            render() {
                const { getFieldDecorator } = this.props.form;
                const { autoCompleteResult } = this.state;

                const formItemLayout = {
                    labelCol: {
                        xs: { span: 24 },
                        sm: { span: 8 }
                    },
                    wrapperCol: {
                        xs: { span: 24 },
                        sm: { span: 16 }
                    }
                };
                const tailFormItemLayout = {
                    wrapperCol: {
                        xs: {
                            span: 24,
                            offset: 0
                        },
                        sm: {
                            span: 16,
                            offset: 8
                        }
                    }
                };

                return (
                    <div style={{ width: "30%" }}>
                        <Card>
                            <Spin spinning={this.props.animals.status == Status.LOADING}>
                            <Form onSubmit={this.handleSubmit}>
                                <FormItem
                                    {...formItemLayout}
                                    label="Starting URL"
                                >
                                    {getFieldDecorator("url", {
                                        rules: [
                                            {
                                                type: "url",
                                                message:
                                                    "The input is not valid url!"
                                            },
                                            {
                                                required: true,
                                                message:
                                                    "Please input your url!"
                                            }
                                        ]
                                    })(<Input />)}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Depth of the tree"
                                >
                                    {getFieldDecorator("depth", {
                                        rules: [
                                            {
                                                required: true,
                                                message:
                                                    "Please select your gender!"
                                            }
                                        ]
                                    })(
                                        <Select
                                            placeholder="Select a option and change input text above"
                                            onChange={this.handleSelectChange}
                                        >
                                            <Option value="genus">Genus</Option>
                                            <Option value="family">
                                                Family
                                            </Option>
                                            <Option value="order">Order</Option>
                                            <Option value="class">Class</Option>
                                            <Option value="phylum">
                                                Phylum
                                            </Option>
                                            <Option value="kingdom">
                                                Kingdom
                                            </Option>
                                        </Select>
                                    )}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Precision of the search"
                                >
                                    {getFieldDecorator("precision", {
                                        initialValue: 1000
                                    })(<InputNumber min={100} max={100000} />)}
                                    <span className="ant-form-text">
                                        {" "}
                                        Pages to crawl
                                    </span>
                                </FormItem>
                                <FormItem {...tailFormItemLayout}>
                                    <Button type="primary" htmlType="submit">
                                        Search
                                    </Button>
                                </FormItem>
                            </Form>
                            </Spin>
                        </Card>
                    </div>
                );
            }
        }
    )
);

//export const CrawlerFormCreated = Form.create()(CrawlerForm);
