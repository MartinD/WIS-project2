import * as React from "react";
import Tree from "react-d3-tree";
import { Card } from "antd";
import { connect } from "react-redux";

export const GraphPage = connect(
    state => {
        return {
            animals: state.animals
      };
    },
    {}
)(
    class Graph extends React.Component {

        render() {

            var treeData =
                {
                    name: "",
                    children: []
                }
            ;

            var animals = this.props.animals.data

            if(this.props.animals.status === "FETCHED"){

            
            animals.forEach((animal, index) => {
                if(animal.kingdom){
                    treeData.name = animal.kingdom;
                    treeData.children.push({
                        name : animal.phylum,
                        children : [
                            {
                                name : animal.classAnimal,
                                children : [
                                    {
                                        name : animal.order,
                                        children : [
                                            {
                                                name : animal.family,
                                                children : [
                                                    {
                                                        name : animal.genus,
                                                        children : [
                                                            {
                                                                name : animal.species
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    })
                } else if (animal.phylum) {
                    treeData.children.push({
                        name : animal.phylum,
                        children : [
                            {
                                name : animal.classAnimal,
                                children : [
                                    {
                                        name : animal.order,
                                        children : [
                                            {
                                                name : animal.family,
                                                children : [
                                                    {
                                                        name : animal.genus,
                                                        children : [
                                                            {
                                                                name : animal.species
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    })
                } 
                else if (animal.classAnimal){
                    //console.log(treeData.children[treeData.children.findIndex(object => object.name == animal.parent)])
                    treeData.children[treeData.children.findIndex(object => object.name === animal.parent)].children.push({
                        name : animal.classAnimal,
                        children : [
                            {
                                name : animal.order,
                                children : [
                                    {
                                        name : animal.family,
                                        children : [
                                            {
                                                name : animal.genus,
                                                children : [
                                                    {
                                                        name : animal.species
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    })
                }
                else if (animal.order){
                    var index1 = treeData.children.findIndex(object => object.name === animal.parent2)
                    var index2 = treeData.children[index1].children.findIndex(object => object.name === animal.parent)
                    treeData.children[index1].children[index2].children.push({
                        name : animal.order,
                        children : [
                            {
                                name : animal.family,
                                children : [
                                    {
                                        name : animal.genus,
                                        children : [
                                            {
                                                name : animal.species
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    })
                } else if (animal.family) {
                    var index1 = treeData.children.findIndex(object => object.name === animal.parent3)
                    var index2 = treeData.children[index1].children.findIndex(object => object.name === animal.parent2)
                    var index3 = treeData.children[index1].children[index2].children.findIndex(object => object.name === animal.parent)
                    treeData.children[index1].children[index2].children[index3].children.push({
                        name : animal.family,
                        children : [
                            {
                                name : animal.genus,
                                children : [
                                    {
                                        name : animal.species
                                    }
                                ]
                            }
                        ]
                    })
                } else if (animal.genus) {
                    var index1 = treeData.children.findIndex(object => object.name === animal.parent4)
                    var index2 = treeData.children[index1].children.findIndex(object => object.name === animal.parent3)
                    var index3 = treeData.children[index1].children[index2].children.findIndex(object => object.name === animal.parent2)
                    var index4 = treeData.children[index1].children[index2].children[index3].children.findIndex(object => object.name === animal.parent)
                    treeData.children[index1].children[index2].children[index3].children[index4].children.push({
                        name : animal.genus,
                        children : [
                            {
                                name : animal.species
                            }
                        ]
                    })
                } else if (animal.species) {
                    var index1 = treeData.children.findIndex(object => object.name === animal.parent5)
                    var index2 = treeData.children[index1].children.findIndex(object => object.name === animal.parent4)
                    var index3 = treeData.children[index1].children[index2].children.findIndex(object => object.name === animal.parent3)
                    var index4 = treeData.children[index1].children[index2].children[index3].children.findIndex(object => object.name === animal.parent2)
                    var index5 = treeData.children[index1].children[index2].children[index3].children[index4].children.findIndex(object => object.name === animal.parent)
                    treeData.children[index1].children[index2].children[index3].children[index4].children[index5].children.push({
                        name : animal.species
                    })
                }                
            })
        }

            return (
                <Card title="Generated Tree" style={{ marginTop : "1rem", height : "27rem" }} className="graph">
                    <Tree data={[treeData]} orientation="vertical"/>
                </Card>
            );
        }
    }
);
