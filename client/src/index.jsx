import { CrawlerFormPage } from "./components/Crawler";
import { GraphPage } from "./components/Graph";
import React from "react";
import ReactDOM from "react-dom";
import { Layout, Menu, Breadcrumb } from "antd";
const { Header, Content, Footer } = Layout;
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";
import { createStore, applyMiddleware, compose, Store } from "redux";

import "antd/dist/antd.less";
import "../style/index.less";

const enhancer = window["devToolsExtension"]
    ? window["devToolsExtension"]()(createStore)
    : createStore;

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(thunk), //allow to dispatch asynchronous actions
        window["devToolsExtension"] ? window["devToolsExtension"]() : f => f
    )
);

ReactDOM.render(
    <Provider store={store}>
        <Layout className="layout" style={{ height : "100%"}}>
            <Header>
                <div className="logo" />
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={["2"]}
                    style={{ lineHeight: "64px" }}
                >
                    <Menu.Item key="1">Home</Menu.Item>
                </Menu>
            </Header>
            <Content style={{ padding: "0 50px" }}>
                <Breadcrumb style={{ margin: "16px 0" }}>
                    {/* <Breadcrumb.Item>Home</Breadcrumb.Item> */}
                </Breadcrumb>
                <div
                    style={{
                        padding: 24,
                        minHeight: "80%",
                        display: "flex",
                        justifyContent: "center",
                        flexDirection : "column",
                        alignItems: "center"
                    }}
                >
                    <CrawlerFormPage />
                    <GraphPage/>
                </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
                Created by Group 1
            </Footer>
        </Layout>
    </Provider>,
    document.getElementById("mount")
);
