import { combineReducers } from "redux";

import animals from "./reducers/animals";

export default combineReducers({
    animals
});
