import axios from "axios";
import { Actions } from "./Actions";
import { Status } from "./Status";

export function fetchAnimals(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_ANIMALS,
            status: Status.LOADING,
            data: []
        });
        return timeout(3600000, axios.post("/api/animals", data))
            .then(res => {
                console.log(res.data);
                dispatch({
                    type: Actions.SET_ANIMALS,
                    status: Status.FETCHED,
                    data: res.data.animals
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_ANIMALS,
                    data: {},
                    status: Status.ERROR
                });
            });
    };
}

function timeout(ms, promise) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            reject(new Error("timeout"));
        }, ms);
        promise.then(resolve, reject);
    });
}
