const express = require("express");
const router = express.Router();
const path = require("path");
const bodyParser = require("body-parser");
const io = require("socket.io")();
const Crawler = require("./crawler.js");

let app = express();

app.use(bodyParser.json());
app.use(function(req, res, next){
    res.setTimeout(3600000, function(){
        console.log('Request has timed out.');
            res.send(408);
        });

    next();
});

let c;

router.post("/", (req, res) => {
    console.log(req.body);
    if(c == null){
        c = new Crawler(
            req.body.url,
            req.body.depth,
            req.body.precision,
            animals => {
                //console.log(animals);
                res.json({animals})
                c = null;
            }
        );
        c.crawl();
    }
});
app.use("/api/animals", router);

// io.on("connection", client => {

//     client.on("generateTree", data => {
//         var c = new Crawler(data.url, data.depth, animals => {
//             client.emit("animals", animals);
//         })
//         c.crawl();
//     })
// });

//io.listen(8081, () => console.log("Listening for incoming user"));
app.listen(8081, () => console.log("Running on localhost:8081"))
