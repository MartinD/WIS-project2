var request = require("request");
var cheerio = require("cheerio");
var URL = require("url-parse");
var xpath = require('xpath')
  , dom = require('xmldom').DOMParser

let pagesVisited;
let numPagesVisited;
let pagesToVisit
let animals
let url;
let baseUrl;


function visitPage(url, depth, callback) {
    // Add page to our set
    pagesVisited[url] = true;
    numPagesVisited++;
    console.log("Number of pages visited :" + numPagesVisited)

    // Make the request
    request(url, (function(error, response, body) {
        if (response.statusCode !== 200) {
            callback();
            return;
        }
        // Parse the document body
        var $ = cheerio.load(body);
        var doc = new dom().parseFromString(body)
        if(searchForWord($, "Species:") && searchForWord($, "Genus:") && searchForWord($, "Family:") && searchForWord($, "Order:") && searchForWord($, "Class:") && searchForWord($, "Phylum:") && searchForWord($, "Kingdom:")){
            console.log("Visiting page " + url);
            if(xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Order:']/td[2]//*[not(*)]", doc)[0] != undefined && xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Species:']/td[2]//*[not(*)]", doc)[0].lastChild != undefined){

            
            var species = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Species:']/td[2]//*[not(*)]", doc)[0].lastChild.data;
            var genus = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Genus:']/td[2]//*[not(*)]", doc)[0].lastChild.data
            var family = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Family:']/td[2]//*[not(*)]", doc)[0].lastChild.data
            var order = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Order:']/td[2]//*[not(*)]", doc)[0].lastChild.data
            var classAnimal = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Class:']/td[2]//*[not(*)]", doc)[0].lastChild.data
            var phylum = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Phylum:']/td[2]//*[not(*)]", doc)[0].lastChild.data
            var kingdom = xpath.select("//table[@class='infobox biota']/tr[td[1] = 'Kingdom:']/td[2]//*[not(*)]", doc)[0].lastChild.data
            var animal = {species, genus, family, order, classAnimal, phylum, kingdom}
            if(animals.length == 0){
                animals.push(animal)
                console.log(animals)
                collectInternalLinks($); 
            }else if(!containsSpecies(animal.species) && containsGenus(animal.genus)){
                if(depth === "genus" || depth === "family" || depth === "order" || depth === "class" || depth === "phylum" || depth === "kingdom"){
                    animals.push({ species : animal.species, parent : animal.genus, parent2 : animal.family, parent3 : animal.order, parent4 : classAnimal, parent5 : animal.phylum })
                    console.log(animals)
                    collectInternalLinks($); 
                }
            }else if(!containsSpecies(animal.species) && containsFamily(animal.family)){
                if(depth === "family" || depth === "order" || depth === "class" || depth === "phylum" || depth === "kingdom"){
                    animals.push({ species : animal.species, genus : animal.genus, parent : animal.family, parent2 : animal.order, parent3 : classAnimal, parent4 : animal.phylum })
                    console.log(animals)
                    collectInternalLinks($);   
                }
            }else if(!containsSpecies(animal.species) && containsOrder(animal.order)){
                if(depth === "order" || depth === "class" || depth === "phylum" || depth === "kingdom"){
                    animals.push({ species : animal.species, genus : animal.genus, family : animal.family, parent : animal.order, parent2 : classAnimal, parent3 : animal.phylum })
                    console.log(animals)
                    collectInternalLinks($);   
                }    
            } else if(!containsSpecies(animal.species) && containsClassAnimal(animal.classAnimal)){
                if(depth === "class" || depth === "phylum" || depth === "kingdom"){
                    animals.push({ species : animal.species, genus : animal.genus, family : animal.family, order : animal.order, parent : classAnimal, parent2 : animal.phylum })
                    console.log(animals)
                    collectInternalLinks($);   
                }
            }else if(!containsSpecies(animal.species) && containsPhylum(animal.phylum)){
                if(depth === "phylum" || depth === "kingdom"){
                    animals.push({ species : animal.species, genus : animal.genus, family : animal.family, order : animal.order, classAnimal : animal, parent : animal.phylum })
                    console.log(animals)
                    collectInternalLinks($);   
                }
            } else if(!containsSpecies(animal.species) && containsKingdom(animal.kingdom)){
                if(depth === "kingdom"){
                    animals.push({ species : animal.species, genus : animal.genus, family : animal.family, order : animal.order, classAnimal : animal, phylum : animal.phylum, parent : animal.kingdom })
                    console.log(animals)
                    collectInternalLinks($);   
                }
            }}
            callback();
        } else {
            // In this short program, our callback is just calling crawl()
            callback();
        }
    }).bind(this) );
}

function collectInternalLinks($) {
    var relativeLinks = $("a[href^='/wiki/']");
    relativeLinks.each(function() {
        var nextPages = []
        pagesToVisit.push(baseUrl + $(this).attr("href"))
    });
}
function searchForWord($, word) {
    var bodyText = $("table[class='infobox biota']")
        .text()
        .toLowerCase();
    return bodyText.indexOf(word.toLowerCase()) !== -1;
}

function containsSpecies(species) {
    return animals.some((animal) => {
        return animal.species === species;
    });
    
}

function containsGenus(genus) {
    return animals.some((animal) => {
        return animal.genus === genus || animal.parent === genus;
    }) || animals.length == 0
}

function containsFamily(family) {
    return animals.some((animal) => {
        return animal.family === family || animal.parent === family;
    }) || animals.length == 0
}

function containsOrder(order) {
    return animals.some((animal) => {
        return animal.order === order || animal.parent === order;
    }) || animals.length == 0
}

function containsClassAnimal(classAnimal) {
    return animals.some((animal) => {
        return animal.classAnimal === classAnimal || animal.parent === classAnimal;
    }) || animals.length == 0
}

function containsPhylum(phylum) {
    return animals.some((animal) => {
        return animal.phylum === phylum || animal.parent === phylum;
    }) || animals.length == 0
}

function containsKingdom(kingdom) {
    return animals.some((animal) => {
        return animal.kingdom === kingdom || animal.parent === kingdom;
    }) || animals.length == 0
}


class Crawler {
    
    constructor(startUrl, depth, precision, getAnimals){
        this.startUrl = startUrl;
        this.depth = depth;
        this.precision = precision;
        this.getAnimals = getAnimals;
        this.init();
        this.crawl = this.crawl.bind(this)       
    }
    
    init(){
        pagesVisited = {}
        numPagesVisited = 0;
        pagesToVisit = [];
        animals = [];
        url = new URL(this.startUrl);
        baseUrl = url.protocol + "//" + url.hostname;
        pagesToVisit.push(this.startUrl);
    }

    crawl() {
        if (pagesToVisit.length == 0 || numPagesVisited > this.precision) {
            console.log("Reached max limit of number of pages to visit.");
            this.getAnimals(animals)
            return;
        }
        var nextPage = pagesToVisit.pop();
        if (pagesVisited[nextPage]) {
            // We've already visited this page, so repeat the crawl
            this.crawl();
        } else {
            // New page we haven't visited
            visitPage(nextPage, this.depth, this.crawl );
        }
    }



    
    

    
    

}

module.exports = Crawler;